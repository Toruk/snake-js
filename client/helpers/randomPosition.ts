import { SCALE, CANVAS_SIZE } from '../constants'
const randomPosition = () => {
    const position = {
        x: Math.floor(Math.random() * (CANVAS_SIZE.x / SCALE - 0) + 0),
        y: Math.floor(Math.random() * (CANVAS_SIZE.y / SCALE - 0) + 0)
    }

    return position
}

export default randomPosition