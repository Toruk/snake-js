import React, { createContext, useState } from 'react'
import { INITIAL_SNAKE } from '../../constants'
import randomPosition from '../../helpers/randomPosition'
import { ICoords } from '../../types/Game'

export const Context = createContext({})

const GameContextProvider: React.FC = ({ children }) => {
    const [ snake, setSnake ] = useState<ICoords[]>(INITIAL_SNAKE)
    const [ apple, setApple ] = useState<ICoords>(randomPosition())
    const [ direction, setDirection ] = useState<string>('down')

    const gameContext = {
        snake,
        setSnake,
        direction,
        setDirection,
        apple,
        setApple
    }

    return (
        <Context.Provider value={gameContext}>
            {children}
        </Context.Provider>
    )
}

export default GameContextProvider