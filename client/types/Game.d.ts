export interface ICoords {
    x: number;
    y: number;
}

export interface IGameContext {
    snake?: ICoords[];
    setSnake?: (snakeCoords: ICoords[]) => void;
    direction?: string;
    setDirection?: (direction: string) => void;
    apple?: ICoords;
    setApple?: (appleCoords: ICoords) => void;
}