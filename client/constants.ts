export const CANVAS_SIZE = {x: 600, y: 600};
export const INITIAL_SNAKE = [
    {x: 5, y: 5},
    {x: 5, y: 5},
];
export const SCALE = 10;
export const SPEED = 100;
export const ALLOWED_KEYS = [
    'ArrowRight',
    'ArrowLeft',
    'ArrowUp',
    'ArrowDown'
];
