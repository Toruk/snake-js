import React, { ReactNode } from "react";
import styled from 'styled-components';

interface IProps {
    onClick: any;
    children: ReactNode;
    className?: string;
}

const BaseButton = styled.button`
    display: inline-block;
    padding: 11px 16px;
    margin: 10px 0;
    min-width: 150px;
    cursor: pointer;
    border: 1px solid transparent;
    border-radius: 2px;
    font-family: 'Montserrat', sans-serif;
    font-size: 14px;
    font-weight: 400;
    text-align: center;
    vertical-align: middle;
    line-height: 1.5;
    background: #3e64ff;
    border-color: #3e64ff;
    color: #fff;
    transition: all 500ms;
    &:hover {
        background: #3f59c3;
        border-color: #3f59c3;
    }
`


function Button({children, onClick, className}: IProps) {
  return <BaseButton onClick={onClick} className={className}>{children}</BaseButton>;
}

export default Button;