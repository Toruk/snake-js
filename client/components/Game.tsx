import React, { useContext, useEffect, useRef, KeyboardEvent } from 'react'
import Styled from 'styled-components'
import { Context } from '../context/Game/context'
import { CANVAS_SIZE, SCALE, ALLOWED_KEYS, SPEED } from '../constants'
import { useInterval } from '../helpers/useInterval'
import randomPosition from '../helpers/randomPosition'
import { ICoords, IGameContext } from '../types/Game'

const Container = Styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    outline: none;
    transform: translate(-50%, -50%);
    canvas {
        background: #000000
    }
`

const Game: React.FC = () => {
    const canvasRef = useRef(null)
    const { snake, setSnake, setDirection, direction, apple, setApple }: IGameContext = useContext(Context)

    const calculateNextPosition = (position: ICoords) => {
        const newPosition = {...position}
        if (direction === 'up') {
            newPosition.y -= 1
        }

        if (direction === 'down') {
            newPosition.y += 1
        }

        if (direction === 'left') {
            newPosition.x -= 1
        }

        if (direction === 'right') {
            newPosition.x += 1
        }

        return canvasEdgeCollision(newPosition)
    }

    const canvasEdgeCollision = (position: ICoords) => {
        if (position.x * SCALE >= CANVAS_SIZE.x) {
            position.x = 0
        }

        if (position.x * SCALE < 0) {
            position.x = CANVAS_SIZE.x / SCALE
        }

        if (position.y * SCALE >= CANVAS_SIZE.y) {
            position.y = 0
        }

        if (position.y * SCALE < 0) {
            position.y = CANVAS_SIZE.y / SCALE
        }

        return position
    }

    const checkAppleCollision = () => {
        const isEatingApple = apple.x === snake[0].x && apple.y === snake[0].y
        
        return isEatingApple
    }
    
    const endGame = () => {
        console.log('End game')
    }

    const gameLoop = () => {
        const snakeCopy = [...snake]
        const nextPosition = calculateNextPosition(snake[0])
        const isTailCollision = checkTailCollision(nextPosition)
        snakeCopy.unshift(nextPosition)

        if (isTailCollision) {
            endGame()
        }

        if (checkAppleCollision() === true) {
            setApple(randomPosition())
        }else{
            snakeCopy.pop()
        }
        setSnake(snakeCopy)
    }

    const checkOppositeDirection = (keyDirection: string) => {
        if (direction === 'up' && keyDirection === 'down') return true
        if (direction === 'down' && keyDirection === 'up') return true
        if (direction === 'left' && keyDirection === 'right') return true
        if (direction === 'right' && keyDirection === 'left') return true

        return false
    }

    const checkTailCollision = (snakeHead: ICoords) => {
        if (snakeHead.x === snake[1].x && snakeHead.y === snake[1].y) return false

        for (const segment of snake) {
            if (snakeHead.x === segment.x && snakeHead.y === segment.y) return true;
        }

        return false
    }

    const moveSnake = (event: KeyboardEvent<HTMLDivElement>) => {
        const { key } = event
        const isArrowKey: boolean = ALLOWED_KEYS.includes(key)
        if (isArrowKey) {
            const arrowKey = key.toLowerCase().replace(/arrow/i, '')
            const isAlreadyMovingInDirection = direction === arrowKey
            const isOppositeDirection = checkOppositeDirection(arrowKey)
            if (isAlreadyMovingInDirection || isOppositeDirection) return

            setDirection(arrowKey)
        }
    }

    useEffect(() => {
        const context = canvasRef.current?.getContext('2d')
        context.setTransform(SCALE, 0, 0, SCALE, 0, 0);
        context.clearRect(0, 0, CANVAS_SIZE.x, CANVAS_SIZE.y);
        context.fillStyle = '#ffffff'
        snake.forEach(({x, y}: ICoords) => context.fillRect(x, y, 1, 1));
        context.fillStyle = '#2ecc71'
        context.fillRect(apple.x, apple.y, 1, 1)
    }, [snake, apple])

    useInterval(() => gameLoop(), SPEED)

    return (
        <div>
            <Container role="button" tabIndex={0} onKeyDown={moveSnake}>
                <canvas ref={canvasRef} width={CANVAS_SIZE.x} height={CANVAS_SIZE.y} />
            </Container>
        </div>
    )
}

export default Game