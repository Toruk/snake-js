
import React from "react";
import ReactDOM from "react-dom";
import { createGlobalStyle } from 'styled-components';
import GameContextProvider from "./context/Game/context";
import Game from './components/Game'

const GlobalStyle = createGlobalStyle`body {
    margin: 0;
    font-family: sans-serif;
    color: #333;
    background: #222f3e;
};`

const App = () => {
    return (
        <div>
            <GlobalStyle />
            <GameContextProvider>
                <Game />
            </GameContextProvider>
        </div>
    )
}

ReactDOM.render(<App />, document.getElementById('root'));
